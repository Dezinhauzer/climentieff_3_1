
#include <GL/glut.h>
#include "math.h"

void drawSquare(float x1, float x2, float y1, float y2) {

    glBegin(GL_POLYGON);
    glVertex2d(x1, y1);
    glVertex2d(x1, y2);
    glVertex2d(x2, y2);
    glVertex2d(x2, y1);
    glEnd();

    glLineWidth(2.5);
    glColor3f(0.0, 0.0, 0.0);
    glBegin(GL_LINES);
    glVertex2d(x1, y1);
    glVertex2d(x1, y2);

    glVertex2d(x1, y2);
    glVertex2d(x2, y2);

    glVertex2d(x2, y2);
    glVertex2d(x2, y1);

    glVertex2d(x2, y1);
    glVertex2d(x1, y1);
    glEnd();
}

void drawPolygon(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {

    glBegin(GL_POLYGON);
    glVertex2d(x1, y1);
    glVertex2d(x2, y2);
    glVertex2d(x3, y3);
    glVertex2d(x4, y4);
    glEnd();

    glLineWidth(2.5);
    glColor3f(0.0, 0.0, 0.0);
    glBegin(GL_LINES);
    glVertex2d(x1, y1);
    glVertex2d(x2, y2);

    glVertex2d(x2, y2);
    glVertex2d(x3, y3);

    glVertex2d(x3, y3);
    glVertex2d(x4, y4);

    glVertex2d(x4, y4);
    glVertex2d(x1, y1);
    glEnd();
}

void drawCycle(float x, float y, float radius, GLubyte r, GLubyte g, GLubyte b) {
    float x1 = x;
    float y1 = y;
    glColor3f(0, 0, 0);
    for (double i = 0; i <= 360;) {
        glBegin(GL_TRIANGLES);
        x = x1 + radius * cos(i);
        y = y1 + radius * sin(i);
        glVertex2d(x, y);
        i = i + .5;
        x = x1 + radius * cos(i);
        y = y1 + radius * sin(i);
        glVertex2d(x, y);
        glVertex2d(x1, y1);
        glEnd();
        i = i + .5;
    }
    glEnd();

    radius -= 0.004f;
    glColor3ub(r, g, b);
    for (double i = 0; i <= 360;) {
        glBegin(GL_TRIANGLES);
        x = x1 + radius * cos(i);
        y = y1 + radius * sin(i);
        glVertex2d(x, y);
        i = i + .5;
        x = x1 + radius * cos(i);
        y = y1 + radius * sin(i);
        glVertex2d(x, y);
        glVertex2d(x1, y1);
        glEnd();
        i = i + .5;
    }
    glEnd();
}

void display() {
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glColor3ub(66, 52, 43);
    drawSquare(0.8f, -0.8f, 0.5f, -0.5f);


    glColor3ub(135, 98, 69);
    drawSquare(0.775f, -0.775f, 0.475f, -0.475f);

    glColor3ub(109, 179, 107);
    drawSquare(0.7f, -0.7f, 0.4f, -0.4f);

    drawCycle(0.7f, 0.4f, 0.05f, 0, 0, 0);
    drawCycle(0.7f, -0.4f, 0.05f, 0, 0, 0);
    drawCycle(-0.7f, 0.4f, 0.05f, 0, 0, 0);
    drawCycle(-0.7f, -0.4f, 0.05f, 0, 0, 0);
    drawCycle(0, 0.4f, 0.05f, 0, 0, 0);
    drawCycle(0, -0.4f, 0.05f, 0, 0, 0);

    drawCycle(-0.6f, .15f, 0.025f, 144, 142, 119);
    drawCycle(-0.6f, .09f, 0.025f, 189, 65, 197);
    drawCycle(-0.6f, .03f, 0.025f, 144, 100, 75);
    drawCycle(-0.6f, -.03f, 0.025f, 38, 185, 201);
    drawCycle(-0.6f, -.09f, 0.025f, 23, 239, 104);

    drawCycle(-0.54f, .12f, 0.025f, 177, 91, 104);
    drawCycle(-0.54f, .06f, 0.025f, 225, 102, 61);
    drawCycle(-0.54f, .0f, 0.025f, 97, 178, 57);
    drawCycle(-0.54f, -0.06f, 0.025f, 239, 231, 42);

    drawCycle(-0.48f, .09f, 0.025f, 37, 41, 192);
    drawCycle(-0.48f, .03f, 0.025f, 106, 125, 131);
    drawCycle(-0.48f, -0.03f, 0.025f, 215, 45, 116);

    drawCycle(-0.42f, .06f, 0.025f, 255, 0, 0);
    drawCycle(-0.42f, 0, 0.025f, 255, 255, 255);

    drawCycle(-0.36f, .03f, 0.025f, 0, 0, 0);

    glColor3ub(123, 94, 76);

    drawPolygon(0, 0,
                -0.005f, 0.015f,
                0.58f, 0.33f,
                0.6f, 0.3f);

    glColor3ub(0, 0, 0);

    drawPolygon(0, 0,
                -0.005f, 0.015f,
                0.05, 0.035f,
                0.05f, 0.035f);

    glFlush();
}


int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitWindowSize(800, 800);
    glutCreateWindow("bulyard");
    glutInitWindowPosition(50, 50);
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}